/* CRUD OPERATION
	- CRUD Operation is the heart of any backend application.
	- mastering the CRUD Operations is essential for any developer.
	- this helps in building character and increasing exposure to logical statements thhat will help us manipulate
	- mastering the CRUD operations of any languages makes us a valuable developer and ,makes the work easier for us to deal with hug amounts of information
*/

// [SECTION] CREATE (Inserting documents)
/*
	- Since MongoDB deals with objects as it's structure for documents, we can easily create them providing objects into our methods.

	- mongoDB shell also uses javascript for it's syntax which means it's convenient for us to understand it's code.
*/
	// Insert ONE Document
	/*
		SYNTAX:
			db.collectionName.insertOne( {objectOne} );
	*/

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		contact:{
			phone: "123456789",
			email: "janedoe@gmail.com"
			},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});

	// Insert MANY Documents
	/*
		SYNTAX:
			db.collectionName.insertMany( [ {objectA} , {objectB} ] );
	*/

	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
				},
			courses: ["Python", "React", "PHP"],
			department: "none"
		}, {
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
				},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	]);

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age : 21,
		contact:{
			phone: "123456789",
			email: "janedoe@gmail.com"
			},
		courses: ["CSS", "JavaScript", "Python"],
	});

	db.users.insertOne({
		lastName: "Doe",
		contact:{
			phone: "123456789",
			email: "janedoe@gmail.com"
			},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		contact:{
			phone: "123456789",
			email: "janedoe@gmail.com"
			},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none",
		gender: "Female"
	});

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		contact:{
			phone: "123456789",
			phone: "123456789011",
			email: "janedoe@gmail.com"
			},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});


// [SECTION] READ (Finding Documents)
	/*
		SYNTAX:
			db.collectionName.find();
			- it will show us all the documents in our collection

			db.collectionName.find({field: value});
			- it will show us the documents that has the given fieldset
	*/
	db.users.find();

	db.users.find({age: 76});
	db.users.find({firstName: "jane"}); // 0 results, case sensitive
	db.users.find({firstName: "Jane"});

	//Finding documents using multiple fieldsets
	/*
		SYNTAX:
			db.collectionName.find( {fieldA:valueA , fieldB:valueB} );
	*/

	db.users.find({lastName: "Armstrong", age:83});


// [SECTION] UPDATE (Updating Documents)
	
	// add document
	db.users.insertOne({
		firstName: "Test",
		lastName: "test",
		age : 0,
		contact:{
			phone: "00000000",
			email: "test@gmail.com"
			},
		courses: [],
		department: "none"
	});

	// updateOne
	/*
		SYNTAX:
			db.collectionName.updateOne({criteria}, {$set: {field:value}});
	*/

	db.users.updateOne({
		firstName:"Jane"
		}, {
			$set: {
				lastName: "Hawking"
			}
	});

	// updating multiple documents
	/*
		SYNTAX:
			db.collectionName.updateMany({criteria}, {$set: {field:value}});
	*/

	db.users.updateMany({
		firstName:"Jane"
		}, {
			$set: {
				lastName: "Wick",
				age: 25
			}
	});

	db.users.updateMany({
		department:"none"
		}, {
			$set: {
				department: "HR"
			}
	});

		// replacing the old document
		/*
			SYNTAX:
				db.collectionName.replaceOne({criteria}, {document/objectToReplace});
		*/

	db.users.replaceOne({firstName: "Test"}, {
		firstName: "Chris",
		lastName: "Mortel"
	});

// [SECTION] DELETE Documents

	// Deleting SINGLE Document
	/*
		SYNTAX:
			db.collectionName.deleteOne({criteria});
	*/
	db.users.deleteOne({firstName: "Jane"});

	// Deleting MANY Documents
	/*
		SYNTAX:
			db.collectionName.deleteMany({criteria});
	*/
	db.users.deleteMany({firstName: "Jane"});

	// REMINDER:
		// db.users.deleteMany({});
	// WILL DELETE WHOLE COLLECTION, DO NOT FORGET TO ADD CRITERIA


// [SECTION] Advanced Queries
	// embedded - objects
	// nested fields - array

	// Query on an embedded document
	db.users.find("contact.phone": "87654321");

	// Query an array with EXACT elements
	db.users.find({courses: ["React","Laravel", "Sass"]});

	// Query oan array without regards to order and elements
	db.users.find({courses: {$all: ["React"]}});




// *deletes whole collection
	db.users.deleteMany({});